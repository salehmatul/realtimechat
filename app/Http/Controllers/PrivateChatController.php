<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Chatroom;

use App\User;
use App\Message;
use App\Events\ChatEvent;


class PrivateChatController extends Controller
{
    public function rtnChatBox($id)
    {   $roomId=Chatroom::where('chatRoomId',$id)->first();
       
        $roomId=$roomId->id;
    
        $message =Message::where('roomId', $roomId)->get();
        $arr = explode(',', $id);
        $senderid = Auth::user()->id;
        $receivers = $this->receivers($senderid);
    

        for ($i = 0; $i < sizeof($arr); $i++) {
            if ($arr[$i] != Auth::user()->id)
                $receiver = $arr[$i];
        }
        $requestmaker=Auth::user()->id;
        
        return view('chats.chatBox')->with('messages', $message)
            ->with('receiverid', $receiver)
            ->with('chatroomUser', $id)
            ->with('receivers', $receivers)
            ->with('requestmaker', $requestmaker)
            ->with('roomId',$roomId);
    }
    public function receivers($id)
    {
        $receiver = array();
        $chatroom = Chatroom::where('chatRoomId', 'Like', '%' . $id . '%')->orderBy('updated_at')->get();
        /*$message = Message::where('chatRoomId','Like','%'.$id.'%')->first();
        dd($message);*/
        foreach ($chatroom as $chat) {
            $arr = explode(',', $chat->chatRoomId);

            for ($i = 0; $i < sizeof($arr); $i++) {
                if ($arr[$i] != $id) {
                    array_push($receiver, User::find($arr[$i]));
                }

            }
        }
        return $receiver;
    }
    public function store (Request $request,$id){
        $senderId = auth()->user()->id;
        $chatroom = Chatroom::where('id', $id)->first();
        $chatRoomId = $chatroom->chatRoomId;
        $arr = explode(',', $chatRoomId);
        for ($i = 0; $i < sizeof($arr); $i++) {
            if ($arr[$i] != Auth::user()->id)
                $receiver = $arr[$i];
        }
        $message = new Message;
        $message->roomId = $chatroom->id;
        $message->sender = $senderId;
        $message->receiver = $receiver;
        $message->readWriteStatus = 0;
        $image=auth()->user()->avatar;
        $user=auth()->user()->name;
        $message->activationStatus = 1;
        $message->message = $request->message;
        $message->selftime=$request->time;
        $message->UTC=$request->utc;
        
        $message->save();
        $wasactive='true';
        event(new ChatEvent($message , $chatroom->id, $image,$user));
        return [
            'id' => $message->id,
             'image'=>$image,
            'wasactive'=>$wasactive,
        ];
    }
    public function timeformate(Request $request){
        $unformatted=$request->untime;
        $unformatted= explode('   ',$unformatted);
        $time= explode(':',$unformatted[0]);
        $date= explode('/',$unformatted[1]);
        $month=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
       
       $m=$date[1];
       $m=$month[$m];
       $d=$date[0];
       $s=$time[2];
       $min=$time[1];
       $h=$time[0];
       $datestring=$m.' '.$d.','.' '.$h.':'.$min;
       return['time'=>$datestring,];
        
    }

}
