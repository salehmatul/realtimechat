<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Message;
use Illuminate\Support\Facades\Hash;
use Session;
use Image;
class testController extends Controller
{
    public function index(){
        $spamedmessageid=array();
        $messages = Message::where('roomId', 7)->get();
         foreach ($messages as $message) {
            if ($message->activationStatus == 0) {
                
            } else {
                
                array_push($spamedmessageid, $message->id);
            }
         }
         print_r($spamedmessageid);
    }
    public function reg()
    {
        return view('auth.register');
    }
    public function regdb(Request $request){
        $user = new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $password= Hash::make($request->password);
        $user->password=$password;
        if($request->hasFile('image')){
            $image=$request->file('image');
            $filename=time().'.'.$image->getClientOriginalExtension();
            $location=public_path('image/'.$filename);
            Image::make($image)->resize(300,300)->save($location);
            $user->avatar=$filename;
        }
        $user->save();
        $id=$user->id;
        return redirect('/dashboard');

    }
    public function local($utcDifference, $utc)
    {
        $utcTimeAndDate = explode(' ', $utc);
        $utcDate = $utcTimeAndDate[0];
        $utcTime = $utcTimeAndDate[1];
        $utcTime = explode(':', $utcTime);
        $utcDate = explode('-', $utcDate);
        $utcDifference = (-1) * $utcDifference;
        $h = $utcDifference / 60;
        $m = $utcDifference % 60;
        $utcTime[1] = $utcTime[1] + $m;
        if ($utcTime[1] >= 60 || $utcTime[1] < 0) {
            if ($utcTime[1] >= 60) {
                $utcTime[1] = $utcTime[1] - 60;
                $h++;
            }
            if ($utcTime[1] < 0) {
                $utcTime[1] = $utcTime[1] + 60;
                $h--;
                $utcTime[1] = $utcTime[1] + $m;
            }

        }
        $utcTime[0] = $utcTime[0] + $h;
        if ($utcTime[0] >= 24 || $utcTime[0] < 0) {
            if ($utcTime[0] >= 24) {
                $utcTime[0] = $utcTime[0] - 24;
                $utcDate[2]++;
            }
            if ($utcTime[0] < 0) {
                $utcTime[0] = $utcTime[0] + 24;
                $utcTime[0] = $utcTime[0] + $h;
                $utcDate[2]--;
            }

        }
        if ($utcDate[2] > 30 || $utcDate[2] < 0) {
            if ($utcDate[2] > 30) {
                $utcDate[1]++;
            }
            if ($utcDate[2] < 0) {
                $utcDate[1]--;
            }

        }
        if ($utcDate[1] > 12 || $utcDate[1] < 0) {
            if ($utcDate[1] > 12) {
                $utcDate[0]++;
            }
            if ($utcDate[1] < 0) {
                $utcDate[0]--;
            }
        }
        $l = $utcDate[0] . '-' . $utcDate[1] . '-' . $utcDate[2] . ' ' . $utcTime[0] . ':' . $utcTime[1] . ':' . $utcTime[2];
        return $l;
    }
   public function ses(){
        $product = [1, 2, 3, 4];
        Session::put('cart', $product);
        Session::push('cart', 5);
        $data= Session::get('cart');
        print_r($data);
   }
   public function logout(){
       $user=Auth::user()->id;
       $user=User::find($user);
       $user->onlineStatus=0;
       $user->save();
        Auth::logout();
       Session::flush();
      
   }
}
