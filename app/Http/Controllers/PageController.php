<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Buyer;
use App\Seller;
use App\Article;
use App\Order;
use App\Query;
use App\SavedPost;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Image;
use Storage;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function wallet()
    {
        $orders = Order::all();
        return view('pages.wallet.index', array('user' => Auth::User(),'orders'=>$orders ));
    }

    public function userAccess()
    {
        $user = User::where('email', Input::get('email'))
            ->orWhere('name', 'like', '%' . Input::get('name') . '%')->get();
        return view('pages.admin.user_access')->withUser($user);
    }

    public function categorySetup()
    {
        return view('pages.admin.category_setup');
    }

    public function queryscreen()
    {
        $data['query_list'] = Query::select("*")->get()->toArray();
        return view('pages.admin.query_screen')->with($data);
    }

    public function takeQuery(Request $request)
    {
        $query = $request -> input('query_command');
        $res = DB::select($query);
        $heading_keys = array_keys(get_object_vars($res['0']));
        $table_headings = "<tr>";
        foreach($heading_keys as $key=>$value)
        {
            $table_headings .= "<th>".$value."</th>";
        }
        $table_headings .= "</tr>";
        $table_to_embed = "";
        if(!empty($res))
        {
            foreach($res as $key => $value)
            {
                $table_to_embed .= "<tr>";
                foreach($heading_keys as $key2 => $value2)
                {
                    $table_to_embed .= "<td>".$value->$value2."</td>";
                }
                $table_to_embed .= "</tr>";
            }
           $data['err'] = "";
        }
        else
        {
            $data['err'] = "query not executed successfully";
        }
        $data['headings'] = $table_headings;
        $data['tabular_data'] = $table_to_embed;
        echo json_encode($data);
    }
    public function saveQuery(Request $request)
    {
        $query_description = $request -> input('query_text');
        $query_name = $request -> input('query_name');
        $data['name'] = $query_name;
        $data['description'] = $query_description;
        $response = Query::create($data);
        $data['success'] = 1;
        echo json_encode($data);
    }
    public function deleteQuery(Request $request)
    {
        $id = $request -> input('query_id');
        $deletedRows = Query::where('id', $id)->delete();
        $data['success'] = $deletedRows;
        echo json_encode($data);
    }
    public function viewAdmin()
    {
        $menu_options = DB::table('menu_options')->orderBy('show_order')->get();
        return view('pages.admin.admin', array('user' => Auth::User(), 'menu_options' => $menu_options) );
    }

    function upload_gif($file){
        $target_dir = "/uploads/avatars/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    }

    public function brandupdate(Request $request)
    {
        if($request->input('update')){
            if($request->hasFile('logo_pic')) {
                $avatar = $request->file('logo_pic');
                $logo_pic = time() . '.' . $avatar->getClientOriginalExtension();
                if($avatar->getClientOriginalExtension() == 'gif'){
                    $avatar->move(public_path('/uploads/avatars/'), $logo_pic);
                }else {
                    Image::make($avatar)->save(public_path('/uploads/avatars/' . $logo_pic));
                }
                DB::table('site_info')->where('attr_name', 'logo_pic')->update(['attr_value' => $logo_pic]);
            }
            if($request->hasFile('header_left_pic')) {
                $avatar = $request->file('header_left_pic');
                $header_left_pic = time() . '.' . $avatar->getClientOriginalExtension();
                if($avatar->getClientOriginalExtension() == 'gif'){
                    $avatar->move(public_path('/uploads/avatars/'), $header_left_pic);
                }else {
                    Image::make($avatar)->save(public_path('/uploads/avatars/' . $header_left_pic));
                }
                DB::table('site_info')->where('attr_name', 'header_left_pic')->update(['attr_value' => $header_left_pic]);
            }
            if($request->hasFile('header_right_pic')) {
                $avatar = $request->file('header_right_pic');
                $header_right_pic = time() . '.' . $avatar->getClientOriginalExtension();
                if($avatar->getClientOriginalExtension() == 'gif'){
                    $avatar->move(public_path('/uploads/avatars/'), $header_right_pic);
                }else {
                    Image::make($avatar)->save(public_path('/uploads/avatars/' . $header_right_pic));
                }
                DB::table('site_info')->where('attr_name', 'header_right_pic')->update(['attr_value' => $header_right_pic]);
            }
            if($request->hasFile('above_footer_pic')) {
                $avatar = $request->file('above_footer_pic');
                $above_footer_pic = time() . '.' . $avatar->getClientOriginalExtension();
                if($avatar->getClientOriginalExtension() == 'gif'){
                    $avatar->move(public_path('/uploads/avatars/'), $above_footer_pic);
                }else{
                    Image::make($avatar)->save(public_path('/uploads/avatars/' . $above_footer_pic));
                }
                DB::table('site_info')->where('attr_name', 'above_footer_pic')->update(['attr_value' => $above_footer_pic]);
            }
            if($request->hasFile('footer_pic')) {
                $avatar = $request->file('footer_pic');
                $footer_pic = time() . '.' . $avatar->getClientOriginalExtension();
                if($avatar->getClientOriginalExtension() == 'gif'){
                    $avatar->move(public_path('/uploads/avatars/'), $footer_pic);
                }else {
                    Image::make($avatar)->save(public_path('/uploads/avatars/' . $footer_pic));
                }
                DB::table('site_info')->where('attr_name', 'footer_pic')->update(['attr_value' => $footer_pic]);
            }
            if($request->input('test_next_to_logo')){
                DB::table('site_info')->where('attr_name', 'test_next_to_logo')->update(['attr_value' => $request->input('test_next_to_logo')]);
            }
            if($request->input('site_name')){
                DB::table('site_info')->where('attr_name', 'site_name')->update(['attr_value' => $request->input('site_name')]);
            }
            if($request->input('site_slogan')){
                DB::table('site_info')->where('attr_name', 'site_slogan')->update(['attr_value' => $request->input('site_slogan')]);
            }
        }
        $site_info = DB::table('site_info')->get();
        $info_element_array = array();
        foreach ($site_info as $info_element){
            $info_element_array[$info_element->attr_name] = $info_element->attr_value;
        }
        return view('pages.admin.brand_update', array('user' => Auth::User(), 'info_element_array' => $info_element_array));
    }


    public function opacityUpdate(Request $request)
    {
        $data = $request->all();
        DB::table('site_info')->where('attr_name', 'form_opacity')->update(['attr_value' => $request->input('data')]);
        $data['success'] = '1';
        echo json_encode($data);
    }

 

    public function userAccessAjax(Request $request){
        $data = $request->all();
        $user = User::where('email', $data['email'])->first();
        if($user->email == Auth::user()->email)
            return "my_email";
        if($user){
            return $user;
        }else{
            return "not_found";
        }
    }
    public function findmenu_options(){
        if(isset($_GET['email']) && trim($_GET['email']) != ""){
            $email = DB::table('users')->where('email', trim($_GET['email']))->get();
            if(isset($email[0])){
                $user_menus = DB::table('user_menu')->where('user_id', $email[0]->id)->get();
                $user_menu_element = array();
                foreach ($user_menus as $user_menu){
                    $user_menu_element[] = $user_menu->menu_options_id;
                }
                $response[0] = $email[0];
                $response[1] = $user_menu_element;
                echo json_encode($response);
            }else{
                echo json_encode(1);
            }
        }else{
            echo json_encode(1);
        }
    }

    public function add_user_menu(){
        if(isset($_POST['find_email']) && trim($_POST['find_email']) != ""){
            $email = DB::table('users')->where('email', trim($_POST['find_email']))->get();
            if(isset($email[0])){
                DB::table('user_menu')->where('user_id', $email[0]->id)->delete();
                if(isset($_POST['menu_options'])){
                    foreach ($_POST['menu_options'] as $menu_options){
                        DB::table('user_menu')->insert(
                            ['user_id' => $email[0]->id, 'menu_options_id' => $menu_options]
                        );
                    }
                    echo json_encode(1);
                }
            }
        }
    }

    public function saveExistingUser(Request $request){
        $data = $request->all();
        if($data['password'])
            $validator =  Validator::make($data, [
                'location' => 'required|string|max:255',
                'phone_no' => 'required|numeric',
                'paypal_email' => 'required|string|email|max:255',
                'password' => 'required|string|min:6|confirmed',
            ]);
        else
            $validator =  Validator::make($data, [
                'location' => 'required|string|max:255',
                'phone_no' => 'required|numeric',
                'paypal_email' => 'required|string|email|max:255',
            ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)
                ->withInput();
        }
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' . $filename ) );
        }
        $user = User::where('email',$request->input('email'))->firstOrFail();
        if($user->email == Auth::user()->email){
            $request->session()->flash('success', 'This is Me!');
            return redirect()->back();
        }
        $user->name          = $request->input('name');
        $user->location          = $request->input('location');
        $user->phone_no          = $request->input('phone_no');
        $user->paypal_email      = $request->input('paypal_email');
        $user->status      = $request->input('status');
        if($data['password'])
            $user->password      = Hash::make($request->input('password'));
        if(isset($filename))
            $user->avatar =$filename;
        $user->save();
        $request->session()->flash('success', 'User created successfully');
        return redirect()->back();

    }
    public function CreateUserWith(Request $request,$flag){

        $user = User::where('email',$request->input('email'))->first();
        /* if($user->email == Auth::user()->email){
            $request->session()->flash('success', 'This is Me!');
            return redirect()->back();
        } */
        if($user){
            $request->session()->flash('success', 'Email already in use');
            return redirect()->back();
        }
        $data = $request->all();
        $validator =  Validator::make($data, [
            'email' => 'required|string|max:255',
            'location' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'phone_no' => 'required|numeric',
            'paypal_email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);


        if($validator->fails()){
            return redirect()->back()->withErrors($validator)
                ->withInput();
        }
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' . $filename ) );
        }
        $user =new User;
        $user->email          = $request->input('email');
        $user->name          = $request->input('name');
        $user->location          = $request->input('location');
        $user->phone_no          = $request->input('phone_no');
        $user->paypal_email      = $request->input('paypal_email');
        $user->password      = Hash::make($request->input('password'));
        if($data['status'])
            $user->status      = $request->input('status');

        if($flag==1)
            $user->email_verified_at = Carbon::now();
        if(isset($filename))
            $user->avatar =$filename;
        $user->save();
        $request->session()->flash('success', 'User created successfully');
        return redirect()->back();
    }
    public function deleteProfile(Request $request){

        $user = User::where('email',$request->input('email'))->first();
        /* if($user->email == Auth::user()->email){
            $request->session()->flash('success', 'This is Me!');
            return redirect()->back();
        } */

        if(!$user){
            $request->session()->flash('success', 'User does not exist');
            return redirect()->back();
        }
        $user->delete();
        $request->session()->flash('success', 'User deleted successfully');
        return redirect()->back();
    }
    public function myPosts(Request $request)
    {
        $id = $request->user()->id;
        $buyers = Buyer::orderBy('id', 'desc')->paginate(10);
        $sellers = Seller::orderBy('id', 'desc')->paginate(10);
        $articles = Article::orderBy('id', 'desc')->paginate(10);
        return view('pages.myposts.index')
        ->withBuyers($buyers)
        ->withSellers($sellers)
        ->withArticles($articles)
        ->withId($id);
    }
    public function savePost()
    {
       $data = array(); 
       $data['user_id'] = $_POST['user_id'];
       $data['post_id'] = $_POST['post_id'];
       $data['post_type']  = $_POST['post_type'];
       if(SavedPost::where('post_id',$data['post_id'])->where('post_type',$data['post_type'])->where('user_id', $data['user_id'])->get()->count() == 0)
       {
          $res = SavedPost::create($data);
          if($data['post_type'] == "buyer")
          {
            $save['buyer_saved_status'] = $_POST['status'];
            Buyer::where('id', $data['post_id'])->update($save);
          }
          if($data['post_type'] == "seller")
          {
            $save['seller_saved_status'] = $_POST['status'];
            Seller::where('id', $data['post_id'])->update($save);
          }
          if($data['post_type'] == "article")
          {
            $save['article_saved_status'] = $_POST['status'];
            Article::where('id', $data['post_id'])->update($save);
          }
          $success = 1;
       }
       else{
           
            $res = SavedPost::where('post_id',$data['post_id'])->where('post_type',$data['post_type'])->where('user_id', $data['user_id'])->delete();
            if($data['post_type'] == "buyer")
            {
              $save['buyer_saved_status'] = $_POST['status'];
              Buyer::where('id', $data['post_id'])->update($save);
            }
            if($data['post_type'] == "seller")
            {
                $save['seller_saved_status'] = $_POST['status'];
                Seller::where('id', $data['post_id'])->update($save);
            }
            if($data['post_type'] == "article")
            {
                $save['article_saved_status'] = $_POST['status'];
                Article::where('id', $data['post_id'])->update($save);
            }
            $success = 0;
           
       }
       $data['success'] = $success ;
       echo json_encode( $res);
    }
    public function getSavePost(Request $request)
    {
        $id = $request->user()->id;
        $posts = SavedPost::where('user_id',$id)->get();
        return view('pages.myposts.saved_posts')->withPosts($posts)->withId($id);;
    }
    public function upcomingServices(){
        return view('pages.upcoming_services.upcoming_services');
    }
}
