

require('./bootstrap');



window.Vue = require('vue');
//file for scroll
import Vue from 'vue'
import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll)

//file for notification
import Toaster from 'v-toaster'
import 'v-toaster/dist/v-toaster.css'
Vue.use(Toaster, { timeout: 5000 })




Vue.component('message', require('./components/message.vue'));


//  const app=new Vue({
// 	el: '#reg',

// render: h => h(App),
// });

const app = new Vue({
	el: '#app',
	data: {
		message: '',
		check: '',
		time: 0,
		utc: 0,
		line:' ',
		levelss: [],
		res:'',
		customlevels: [],
		selected: ' ',
		
		chat: {
			messages: [],
			user: [],
			color: [],
			time: [],
			messageid: [],
			spam:[],
			report:[],
			onlineUserId:[],
			mgssender: [],
			spamedmessageid:[],
			images:[],
			
		},
		typing: '',
		online: ' ',
		iid: 0
	},
	methods: {
		send(id){

			if (this.message.length != 0) {
				this.chat.messages.push(this.message);
				this.chat.spamedmessageid.push('false');
				this.chat.mgssender.push('true');
				this.chat.user.push('Me');
				this.chat.color.push("mesuccess");
				var k = this.dateformate();
				this.chat.time.push(k);
				this.chat.spam.push('none');
				this.chat.report.push('none');
				this.time = this.getTime();
				this.utc = this.getTimezone();
				//console.log(this.time);
				
				axios.post('/send/' + id, {
					
					message: this.message,
					time: this.time,
					utc:this.utc,

				})
					.then(response => {
						this.message = '';
						
						this.chat.messageid.push(response.data.id);
						this.chat.images.push(response.data.image) ;
						
					})
					.catch(error => {
						console.log(error);
					});
			}

		},
		setlevel(id){
			
			
			
				console.log(this.selected);
				console.log(roomid);
				if(this.selected != ''){
					this.levelss.push(this.selected);
				}
				axios.post('/levelset', {
					
						roomid: id,
						levels:this.selected,
					})
					.then(response => {
						//this.selected='';
						console.log(response.data);
						this.levelss.push(response.data.level);
						
					})
					.catch(error => {
						console.log(error);
					});

			
			
		},
		dateformate(){
			var d=this.getTime();
			
			axios.post('/timeformate',{
				untime:d,
			})
			.then(response=>{
				this.res=response.data.time;
				//console.log(response.data.time);
			})
			.catch(error =>{
				console.log(error);
			});
			//console.log(this.res);
			return this.res;

		},
		
		
		getTime() {
			
			var currentTime = new Date();
			var hours = currentTime.getHours();
			var minutes = currentTime.getMinutes();
			var second=currentTime.getSeconds();
			var year = currentTime.getFullYear();
			var month = currentTime.getMonth();
			var date = currentTime.getDate();
			
			if (minutes < 10) {
				minutes = "0" + minutes;
			}
			//console.log(month);
			let tim = hours + ':' + minutes + ':' + second + '   '+ date + '/' + month + '/' + year ;
			
			return tim;

		},
		getTimezone(){
		   var currentTime = new Date();
		   var utc = currentTime.getTimezoneOffset();
		   return utc;
		   
		},
		getOldMessages() {
			
			axios.post('/getOldMessage', {
				room: fetchroomId,
				me: requestmaker,
				time: this.getTime(),
				utc:this.getTimezone(),
			})
				.then(response => {
					console.log(response.data);
					this.chat.messages = response.data.messages;
					this.chat.color = response.data.color;
					this.chat.time = response.data.time;
					this.chat.user = response.data.user;
					this.chat.messageid = response.data.messageid;
					this.chat.images=response.data.image;
					this.chat.spam = response.data.spam;
					this.chat.report=response.data.report;
					this.chat.spamedmessageid = response.data.spamedmessageid;
					this.line = response.data.receiverOnline;
					this.chat.mgssender = response.data.mgssender;
					this.cont=response.data.cont;
				})
				.catch(error => {
					console.log(error);
				});
		},
		getOldLevel(){
			axios.post('/getOldLevel',{
				roomid: fetchroomId,
				

			})
			.then(response=>{
					this.levelss=response.data.levels;
			})
			.catch(error=>{
				console.log(error);
			});
		},
		geallOnlineUser(){
			axios.post('/getallOnlineUser',{

			})
			.then(response =>{
				this.chat.onlineUserId.push(response.data.onlineUserId);
			})
			.catch(error =>{
				console.log(error);
			});
		}
		
	},
	created(){
		Echo.channel('online')
			.listen('OnlineEvent', e => {
				// console.log('ami sob sonsiree vai');
			 console.log(e.user.id);
				this.line='online';
				axios.post('/setonline' ,{
					id: e.user.id,
				})
				.then(response=> {
					this.line='Online Now';
				})
				.catch(error =>{
					console.log(error);
				});

			});
	},
	
	watch:{
		message(){
			Echo.private('chat-roomId-' + fetchroomId)
			    .whisper('typing', {
			        name: this.message
			    });
		},
		
	},
	mounted() {
		this.getOldMessages();
		this.getOldLevel();
		var m=this.getTime();
		//this.dateformate(m);
		//console.log(this.dateformate(m));

		//this.check=fetchroomId;
		
		//
		//console.log(this.chat.spamedmessageid);
		//this.readwrite();


		console.log(requestmaker);
		Echo.private('chat-roomId-' + fetchroomId)
			.listen('ChatEvent', (e) => {
				console.log(e);
				this.chat.messages.push(e.message.message);
				this.chat.user.push(e.user);
				this.chat.color.push("warning");
				this.chat.time.push(this.getTime());
				this.chat.messageid.push(e.message.id);
				this.chat.spam.push('block');
				this.chat.spamedmessageid.push('false');
				this.chat.mgssender.push('false');
				this.chat.images.push(e.image);

			})
			.listenForWhisper('typing', (e) => {
				if (e.user != '')
					this.typing = "typing...";
				else
					this.typing = "";
			});
			

	}
});
