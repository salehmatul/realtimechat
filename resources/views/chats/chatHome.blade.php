<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'user' =>  auth()->user()
        ]) !!};
        var fetchChatURL = null;
    </script>

   
</head>
<body >
 <script>
   var roomid="{{  $roomId }}";
 </script>   
   
<div class="container divbox"  id="app">

    <div style="float:letf" class="col-md-4 float-left" id="reg">

        <div class="" >
            <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               All conversation
            </a> 
    
            <input type="text" placeholder="Search with name" class="form-controller" id="search" name="search"></input>
    
           

            <div class="dropdown-menu " >
              
                <li class="dropdown-item mydrp" href="#">Unread</li>
                
                <hr>
                
                    <li class="dropdown-item" href="#">Archive</li>
                        <li  class="dropdown-item "  id="sss">Spam</li>
                       
                
                    <li class="dropdown-item" href="#">Report</li>
                
                <hr>
                
                <li class="btn dropdown-toggle" href="#" role="button" id="lll" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Levels..
                </li> 
                <div class="dropdown-menu" >
                        <table class="table table-bordered table-hover well-sm">
        
                                
                            <tbody  id="levelbody">

                            </tbody>
                                
                        </table>
                        
                </div>
                 
            
            </div>
            
           <div style="overflow-y: scroll ;width:100%">
                    <table class="table table-bordered table-hover" >
                        
                                                
                        <tbody  id="spambody" >
                
                        </tbody>
                                                
                    </table>
                </div>
        
           
            <table class="table table-bordered table-hover text-success" >
           
              
                <tbody id="tbod">
        
                </tbody>
              
            </table>

        </div>
 
 
       
        <ul class="table " style="overflow-y: scroll;height:500px ;width:100%" v-chat-scroll> 
             
          
                
           
            @foreach($receivers as $receiver)
           
            
                @php
                    $userid = Auth::user()->id;
                    $receiverid = $receiver->id;
                    if($userid==$receiverid){
                        continue;
                    }
                
                    if($receiverid > $userid){
                    $chatRoomId = Auth::user()->id.','.$receiver->id;
                    }
                    else{
                    $chatRoomId = $receiver->id.','.$userid;
                    }
                    $romid=App\Chatroom::where('chatRoomId',$chatRoomId)->first();
                    $romid=$romid->id;
                @endphp
              
                
                
                   
                    @php
                    
                       $messagecont=App\Message::where('RoomId',$romid)
                                                ->where('readWriteStatus','!=',1)
                                                ->where('sender','!=',$userid)
                                                ->count();
                        $message='';
                        if($messagecont>0){
                            $messageunread = App\Message::where('RoomId',$romid)
                                                ->where('readWriteStatus','!=',1)
                                                ->where('sender','!=',$userid)
                                                ->orderBy('created_at','DESC')
                                                ->first();
                        }                     
                        else{
                            $message = App\Message::where('RoomId',$romid)->orderBy('created_at','DSEC')->first();
                            $messageunread=0;
                        }
                        
                   $timestring=' ';
                   //dd($message);
                    if($message!=''){
                         $time = strtotime($message->created_at);
                        $date= date('Y-m-d H:i:s', $time);
                        $date = date_create($date);
                        $nowdate = date("Y-m-d H:i:s");
                        $nowdate = date_create($nowdate);
                        $diff = date_diff($nowdate, $date);
                       
                        if($diff->s > 0 && $diff->i <1  && $diff->h <= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0){
                            $timestring='just now';
                        }elseif ($diff->i >=1  && $diff->h <= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0) {
                        $timestring=$diff->i.' min ago';
                        }elseif ($diff->i >=1  && $diff->h >= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0) {
                            $timestring=$diff->h.' hour'.$diff->i.' min ago';
                        }elseif($diff->h >= 0 && $diff->d >=0 && $diff->m <=0 && $diff->y <=0){
                            $timestring=$diff->d.' days '.$diff->h.' hour ago';
                        }elseif($diff->d >=0 && $diff->m >=0 && $diff->y <=0){
                            $timestring=$diff->m.' months '.$diff->d.' days ago';
                        }
                        elseif ($diff->m >=0 && $diff->y >=0) {
                        $timestring=$diff->y.' year '.$diff->m.' months ago';
                        }
                    }

                    if($messageunread){
                         $time = strtotime($messageunread->created_at);
                        $date= date('Y-m-d H:i:s', $time);
                        $date = date_create($date);
                        $nowdate = date("Y-m-d H:i:s");
                        $nowdate = date_create($nowdate);
                        $diff = date_diff($nowdate, $date);
                       
                        if($diff->s > 0 && $diff->i <1  && $diff->h <= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0){
                            $timestring='just now';
                        }elseif ($diff->i >=1  && $diff->h <= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0) {
                        $timestring=$diff->i.' min ago';
                        }elseif ($diff->i >=1  && $diff->h >= 0 && $diff->d <=0 && $diff->m <=0 && $diff->y <=0) {
                            $timestring=$diff->h.' hour'.$diff->i.' min ago';
                        }elseif($diff->h >= 0 && $diff->d >=0 && $diff->m <=0 && $diff->y <=0){
                            $timestring=$diff->d.' days '.$diff->h.' hour ago';
                        }elseif($diff->d >=0 && $diff->m >=0 && $diff->y <=0){
                            $timestring=$diff->m.' months '.$diff->d.' days ago';
                        }
                        elseif ($diff->m >=0 && $diff->y >=0) {
                        $timestring=$diff->y.' year '.$diff->m.' months ago';
                        }
                    }
                   

                    //echo $timestring;
                    @endphp
                    
                     
                    @if( $messageunread)
                     <li style="overflow: auto;background:lightgray;">
                            <img style="border-radius: 50%;display:inline" src="{{asset('/uploads/avatars'.$receiver->avatar)}}" height="50px" width="50px" alt="default.png"  >
                            <h3  style="display:inline" @keyup.enter='dataset{{($romid)}}'> 
                            <a class="alink" href="{{url('privateChat/'.$chatRoomId)}}">   {{$receiver->name}}</a>
                            </h3> 
                            <i class="far fa-star"></i>
                            <i class="" style=" color:rgb(153, 153, 153);" >{{$timestring}}</i>
                            <div class="divmgs">
                            <h5 class="offset-1 well-sm" style="display:inline;font-weight:700;font-size: 18px;" ><b>{{ $messageunread->message}}</b></h5>
                            <span class="badge badge-success">{{$messagecont}}</span>
                            </div>
                        </li>
                       @elseif($message)
                        <li style="overflow: auto;">
                                <img style="border-radius: 50%;display:inline" src="{{asset('/uploads/avatars'.$receiver->avatar)}}" height="50px" width="50px" alt="default.png"  >
                                <h3  style="display:inline" @keyup.enter='dataset{{($romid)}}'> 
                                <a class="alink" href="{{url('privateChat/'.$chatRoomId)}}">   {{$receiver->name}}</a>
                                </h3> 
                                <i class="far fa-star"></i>
                                <i class="" style=" color:rgb(153, 153, 153);">{{$timestring}}</i>
                               <div class="divmgs"> <h5 class="offset-2" style="display:inline;font-size: 13px;" >{{ $message->message}}</h5></div>
                        </li>
                                            
                    @endif
                   
                   
           
          
           <hr>
             @endforeach 
            
             
        </ul>

    </div>
    @yield('content')
    

      

   
</div>
   
    <!-- Scripts -->
    <script type="text/javascript">
         @yield('routes')

    </script>
   
    @yield('script')

   

   
    <script src="{{ asset('js/app.js') }}"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script type="text/javascript">

            $('#search').on('keyup',function(){
            
                 $value=$(this).val();
            
                 $.ajax({
            
                type : 'get',
            
                 url : '{{URL::to('search')}}',
            
                 data:{'search':$value},
            
                 success:function(data){
            
                  $('#tbod').html(data);
            
                 }
            
                 });
        
            })
        
        </script>
        <script type="text/javascript">
        $(document).ready(function(){
            $("#sss").click(function(){
                $.ajax({
                    type: 'get',
                    url : '{{URL::to('spamsearch')}}',
                   // data:{'authid':$userid},
                    success:function(data){
                        $('#spambody').html(data);
                      
                    }
                })
            });
            });
        </script>
        <script type="text/javascript">
        $(document).ready(function(){
            $("#lll").click(function(){
                $.ajax({
                    type: 'get',
                    url : '{{URL::to('levelsearch')}}',
                    data:{'authid':roomid },
                    success:function(data){
                     $('#levelbody').html(data);
                       console.log('success');
                    }
                })
            });
            });
        </script>
        <script type="text/javascript">
        $(document).click(function(){
            $("#spamclose").click(function(){
                $.ajax({
                    type: 'get',
                    url : '{{URL::to('spamsearch')}}',
                    data:{'close':'close'},
                    success:function(data){
                     $('#spambody').html(data);
                       console.log('success');
                    }
                })
            });
            });
        </script>
    
         
     
       
</body>
</html>
