@extends('chats.chatHome')
@section('routes')
var fetchroomId = "{{  $roomId }}";
var requestmaker ="{{ $requestmaker }}";
@endsection
@section('content')
    @php
       $u=Auth::user()->id;
       $receiver_user=App\User::where('id',$receiverid)->first();  
       $mes=App\Message::where('RoomId',$roomId) 
                        ->where('readWriteStatus','!=',1)
                         ->where('sender','!=',$u)
                          ->get(); 
        foreach ($mes as $mes) {
           $mes->readWriteStatus=1;
           $mes->save();
        }

    @endphp

    <div class=" col-md-8 float-right"  >
			
          
           
            
            <div class="col-md-2 float-left" style="margin-right:0">
                <tr>  <img style="border-radius: 50%;display:inline;padding:3px" src="{{asset('/uploads/avatars'.$receiver_user->avatar)}}" height="60px" width="60px" alt="default.png"  > </tr> 
            </div>
            <div class="">
               <table style="width:70%;" >
                    <tr scope="row" >
                        <td colspan="12"><h1 class="chtbxusername">{{$receiver_user->name}}</h1></td>
                        <td style=" empty-cells: show">
                     
                        </td>
                      
                        <td style="float: right;">
                            <i style="padding-right:10px" class="far fa-star "></i>
                            <i style="padding-right:10px" class="far fa-envelope-open"></i>
                            <i style="padding-right:10px" class="fas fa-trash-alt"></i>
                           

                            <a  href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i style="padding-right:10px" class="fas fa-tag"></i></a>

                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                  
                                        <input type="radio" id="jack" value='Follow' v-model="selected" @click="setlevel({{$roomId}})">
                                        <label for="jack">Follow-Up</label>
                                        <br>
                                        <input type="radio" id="john" value='Nudge' v-model="selected" @click="setlevel({{$roomId}})">
                                        <label for="john">Nudge</label>
                                        <br>
                                        <form method="POST" action="{{url('/addcustomlevel')}}">
                                            {{ csrf_field() }}
                                            <input type="text" name='customlevel' placeholder="Custom level">
                                            <br>
                                            <input type="submit" class="btn" value="Add custom level">
                                            <input type="hidden" name="roomid" value="{{$roomId}}">
                                        </form>
                                        
                                     
                                    

                                </div> 
                            
                            <i class="fas fa-book"></i>@{{selected}}
                        </td>
                    </tr>
                    <tr scope="row">
                    <td> @{{line}}   | Local time: <i id="demo"></i></td>
                    </tr>
                    <tr >
                        <td>Level:
                     <li class="well-sm" style="display:inline;" v-for="value in levelss"
                       
                     >
                    @{{value}}
                        </li>
                    </td>
                    </tr>
               </table>

            </div>
           
            


			<span style="color:orange;">@{{typing}}</span>
			<ul style="overflow-y: scroll;height:440px" class="list-group " v-chat-scroll >
			  
			  <message
			  	v-for="value,index in chat.messages"
			  	:key=value.index
			  	:user=chat.user[index]
			  	:color=chat.color[index]
			  	:time=chat.time[index]
                :messageid=chat.messageid[index]
                :spam=chat.spam[index]
                :report=chat.report[index]
                :images=chat.images[index]
                :mgssender=chat.mgssender[index]
                :spamedmessageid=chat.spamedmessageid[index]

               
			  >@{{value}} </message>
			  
            </ul>
			<input type="text" placeholder="type your message.." class="form-control" v-model="message" @keyup.enter="send({{$roomId}})">
			
	</div>
@endsection
@section('script')
<script>
    var myVar = setInterval(myTimer, 1000);
   

    function myTimer() {
    var d = new Date();
    document.getElementById("demo").innerHTML = d.toLocaleTimeString();
    }

</script>
@endsection
<script>
    
</script>