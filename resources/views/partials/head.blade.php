<?php
use Illuminate\Support\Facades\DB;
$site_info = DB::table('site_info')->get();
$info_element_array = array();
foreach ($site_info as $info_element){
    $info_element_array[$info_element->attr_name] = $info_element->attr_value;
}
?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="/uploads/avatars/{{$info_element_array['logo_pic']}}"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$info_element_array['test_next_to_logo']}}</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Dropzone Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Custom Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link href="{{ asset('css/tableexport.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <style> 
        .hide{
            display: none;
        }
        .m-t-12{
            margin-top: 12px;
        }
        .p-abs{
            position: absolute;
        }
        .t-36{
            top:36%;
        }   
        .show-ic:hover .image-upload-icon{
            opacity:1 !important;
        } 
        .query .table .left-one{
        width: 119px;
        }
        .query tr.frst-one {
            background-color:#948a54;
            color:#000;
        }

        .query tbody tr td {
            padding: 16px 0px;
        }

        .query tbody tr td.first-row {
            padding: 12px 0px;
        }

        .query tr td img{
            width: 18px;
            float: right;
            margin-right: 21px;
        }
        .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
            border:1px solid #000;
        }

        .queryscreen .main-for-back{
            background-color: #e1dfe3;
            padding: 8px 28px;
            border-top: 1px solid #939394;
            border-bottom: 1px solid #939394;
        }

        .queryscreen .main-for-back .main-content-left {
            float: right;
        }

        .queryscreen .main-for-back .main-content-left a.btn {
            padding: 0px;
            margin-right:20px;
        }

        .queryscreen .main-for-back .main-content-left a.btn i {
            font-size: 19px;
        }
        .queryscreen .main-content h3{
            margin:0px;
        }
        .textarea-sec textarea{
            width:100%;
            overflow-y:scroll;
            overflow-x:scroll;
            border-top: 3px solid #dcd9d9;
            border-left: 3px solid #dcd9d9;
        }

        .textarea-sec {
            border: 1px solid #000;
            padding: 6px 6px;
        }

        .table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td{
            border: 1px solid #dedada;
        }
        tr.f-row {
            background-color:#e1dfe3;
        }
        tr.f-row th{
            color: #000;
        }

        .table-dummy {
            height: 500px;
            overflow-x: scroll;
            overflow-y: scroll;
        }
        .table-dummy table{
            border-top: 3px solid #aba8a8;
            border-left: 3px solid #aba8a8;
        }

        i.fa-play {
            color:#0b8107;
        }
        i.fa-download {
            color:#000;
        }
        i.fa-floppy-o {
            color:#8255b1;
        }
        .m-l-79{
            margin-left:79px;
        }
        .w-40{
            width:40%;
        }
        .m-r-35{
            margin-right:35px !important;
        }
        .m-r-19{
            margin-right:19px !important;
        }
        .m-l-12{
            margin-left:12px;
        }
        img:hover {
            cursor: pointer;
        }
        .description{
            cursor: pointer;
        }
        .fl-r{
            float: right;
        }
        .crs-pntr{
            cursor: pointer;
        }
        .m-t-25{
            margin-top:25px;
        }
        .p-l-12{
            padding-left: 12px;
        }
        h2{
  text-align:center;
  padding: 20px;
}
/* Slider */

.slick-slide {
    margin: 0px 20px;
}

.slick-slide img {
    width: 100%;
}

.slick-slider
{
    position: relative;
    display: block;
    box-sizing: border-box;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
            user-select: none;
    -webkit-touch-callout: none;
    -khtml-user-select: none;
    -ms-touch-action: pan-y;
        touch-action: pan-y;
    -webkit-tap-highlight-color: transparent;
}

.slick-list
{
    position: relative;
    display: block;
    overflow: hidden;
    margin: 0;
    padding: 0;
}
.slick-list:focus
{
    outline: none;
}
.slick-list.dragging
{
    cursor: pointer;
    cursor: hand;
}

.slick-slider .slick-track,
.slick-slider .slick-list
{
    -webkit-transform: translate3d(0, 0, 0);
       -moz-transform: translate3d(0, 0, 0);
        -ms-transform: translate3d(0, 0, 0);
         -o-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
}

.slick-track
{
    position: relative;
    top: 0;
    left: 0;
    display: block;
}
.slick-track:before,
.slick-track:after
{
    display: table;
    content: '';
}
.slick-track:after
{
    clear: both;
}
.slick-loading .slick-track
{
    visibility: hidden;
}

.slick-slide
{
    display: none;
    float: left;
    height: 100%;
    min-height: 1px;
}
[dir='rtl'] .slick-slide
{
    float: right;
}
.slick-slide img
{
    display: block;
}
.slick-slide.slick-loading img
{
    display: none;
}
.slick-slide.dragging img
{
    pointer-events: none;
}
.slick-initialized .slick-slide
{
    display: block;
}
.slick-loading .slick-slide
{
    visibility: hidden;
}
.slick-vertical .slick-slide
{
    display: block;
    height: auto;
    border: 1px solid transparent;
}
.slick-arrow.slick-hidden {
    display: none;
}
        


    </style>


    @yield('custom-styles')
</head>
