@extends('layouts.app')

@section('content')
<section id="userAccess" class="userAccess">
  <div class="container-fluid mt-5 mb-3">
    <div class="col-md-10 offset-md-1">
      <div class="card shadow-lg">
        <div class="card-header">
          <a href="{{ URL('/home') }}"><i class="fa fa-times-circle fl-r crs-pntr" style="font-size:27px;color:#6c757d;"></i></a>
          <h5 class="card-title pt-3">Category Setup</h5>
        </div>
        <div class="card-body">
          <div class="form-row">
            <div class="form-group col-md-4 pr-5">
              <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action">
                  Popular
                </a>
                <a href="#" class="list-group-item list-group-item-action">Food</a>
                <a href="#" class="list-group-item list-group-item-action">Technology</a>
                <a href="#" class="list-group-item list-group-item-action">Fashion</a>
                <a href="#" class="list-group-item list-group-item-action">Art</a>
              </div>
            </div>
            <div class="form-group col-md-8">
              <div class="form-group row">
                <label for="parent_category" class="col-form-label col-md-4">Parent Category</label>
                <div class="col-md-6">
                  <input type="text" class="form-control{{ $errors->has('parent_category') ? ' is-invalid' : '' }}"
                  name="parent_category" value="{{ old('parent_category') }}" required>
                </div>
              </div>
              <div class="form-group row">
                <label for="category_image" class="col-sm-4 col-form-label"></label>
                <div class="col-sm-6">
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="category_image" aria-describedby="category_image">
                      <label class="custom-file-label" for="category_image">Choose image</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label for="category_search" class="col-sm-4 col-form-label"></label>
                <div class="col-sm-6">
                  <div class="input-group">
                    <input class="form-control" type="search" placeholder="">
                    <div class="input-group-append">
                     <button class="btn btn-secondary" type="button" id="category_search">
                       <i class="fas fa-search"></i>
                     </button>
                   </div>
                 </div>
                </div>
              </div>
              <div class="form-group row">
                <label for="category_name" class="col-sm-4 col-form-label">Category Name</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control{{ $errors->has('category_name') ? ' is-invalid' : '' }}"
                  name="category_name" value="{{ old('category_name') }}" required>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-4">
                  <a href="#" class="btn btn-success btn-block">Edit</a>
                </div>
                <div class="col-md-4">
                  <a href="#" class="btn btn-danger btn-block">Delete</a>
                </div>
                <div class="col-md-4">
                  <a href="#" class="btn btn-success btn-block">Add</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('extra-JS')

@endsection
